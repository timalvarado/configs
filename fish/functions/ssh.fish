function ssh --description 'OpenSSH SSH client (remote login program) with a conservative $TERM value'
  switch $TERM
    case xterm-termite
      set -lx TERM xterm
      command ssh $argv
    case '*'
      command ssh $argv
  end
end
