syntax on
filetype indent plugin on

set nocompatible
set wildmenu
set showcmd
set ignorecase
set smartcase
set ruler
set cmdheight=2
set number
set shiftwidth=4
set tabstop=4
set softtabstop=4
set expandtab
set showmatch   " highlight matching [{()}]
set incsearch
set hlsearch
set backspace=indent,eol,start

" Color settings
autocmd BufEnter *.py colorscheme nord
hi Visual term=reverse cterm=reverse guibg=White

" Copy and Paste
noremap <leader>y "*y
noremap <leader>p "*p
noremap <leader>Y "+y
noremap <leader>P "+p

" Key remappings
let mapleader = ','

nnoremap <C-p> :<C-u>FZF<CR>

" Plugin Management

" Adding plugins
" set rtp+=~/.dotfiles/vim/.vim/pack/minpac/opt/minpac
packadd minpac

" Minpac
call minpac#init()

call minpac#add('rust-lang/rust.vim')
call minpac#add('pearofducks/ansible-vim')
call minpac#add('fatih/vim-go')
call minpac#add('sheerun/vim-polyglot')
call minpac#add('tpope/vim-unimpaired')
call minpac#add('vim-jp/vim-go-extra')
call minpac#add('arcticicestudio/nord-vim')
call minpac#add('preservim/nerdtree')

" vim-grepper
let g:grepper = {}
let g:grepper.tools = ['grep', 'git', 'rg']

" Search for the current word
nnoremap <leader>g :Grepper<CR>

" Nerdtree
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
let NERDTreeShowHidden=1 " Shows hidden files

" Nerdtree keybindings
let g:NERDTreeDirArrowExpandable = '▸'
let g:NERDTreeDirArrowCollapsible = '▾'

" Golang
autocmd FileType go autocmd BufWritePre <buffer> Fmt


" Syntax Highlighting - Syntastic
set statusline+=%#warningmsg#
set statusline+=%{SyntasticStatuslineFlag()}
set statusline+=%*

let g:syntastic_always_populate_loc_list = 1
let g:syntastic_auto_loc_list = 1
let g:syntastic_check_on_open = 1
let g:syntastic_check_on_wq = 0
