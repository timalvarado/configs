if status is-interactive
and not set -q TMUX
    exec tmux
end

# Greeting message
set --erase fish_greeting


set -g man_blink -o red
set -g man_bold -o green
set -g man_standout -b black 93a1a1
set -g man_underline -u 93a1a1

# ENV variables
set -gx PATH $PATH ~/.local/bin ~/.cargo/bin /usr/local/go/bin
#set -gx FZF_DEFAULT_COMMAND  'rg --files'
#set -gx PASSWORD_STORE_DIR ~/.dotfiles/pass
#set -gx PASSWORD_STORE_ENABLE_EXTENSIONS true
set -gx EDITOR /usr/bin/vim
#set -gx POWERSHELL_TELEMETRY_OPTOUT 1

# Docker-machine ENV variables
#set -gx DOCKER_TLS_VERIFY "1";
#set -gx DOCKER_HOST "tcp://192.168.99.102:2376";
#set -gx DOCKER_CERT_PATH "/home/tim/.docker/machine/machines/dswarm01";
#set -gx DOCKER_MACHINE_NAME "dswarm01";

#eval (python3.7 -m virtualfish)

# Virtualenv
#set -gx VIRTUALFISH_HOME ~/code/work
