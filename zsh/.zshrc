export ZSH="/home/tim/.oh-my-zsh"
ZSH_THEME="mh"
plugins=(
  git
  python
)

source $ZSH/oh-my-zsh.sh

# Env Variables
export PATH=/snap:$PATH
export FZF_DEFAULT_COMMAND='rg --files'
export Path=/home/tim/.cargo/bin:$PATH
export PATH=/home/tim/code/virnetiks/shopping-bot/drivers:$PATH
export PASSWORD_STORE_DIR=/home/tim/.dotfiles/pass
export PASSWORD_STORE_ENABLE_EXTENSIONS_DIR=true
#export TERM=xterm-color
