#  ____ _____ 
# |  _ \_   _|  Derek Taylor (DistroTube)
# | | | || |    http://www.youtube.com/c/DistroTube
# | |_| || |    http://www.gitlab.com/dwt1/ 
# |____/ |_|  	

;=====================================================
;
; To learn more about how to configure Polybar
; go to https://github.com/jaagr/polybar
;
; The README contains alot of information
; Themes : https://github.com/jaagr/dots/tree/master/.local/etc/themer/themes
; https://github.com/jaagr/polybar/wiki/
; https://github.com/jaagr/polybar/wiki/Configuration
; https://github.com/jaagr/polybar/wiki/Formatting
;
;=====================================================

[global/wm]
;https://github.com/jaagr/polybar/wiki/Configuration#global-wm-settings
margin-top = 0
margin-bottom = 0

[settings]
;https://github.com/jaagr/polybar/wiki/Configuration#application-settings
throttle-output = 5
throttle-output-for = 10
throttle-input-for = 30
screenchange-reload = true
compositing-background = over
compositing-foreground = over
compositing-overline = over
compositing-underline = over
compositing-border = over

; Define fallback values used by all module formats
format-foreground = #FF0000
format-background = #00FF00
format-underline =
format-overline =
format-spacing =
format-padding =
format-margin =
format-offset =

[colors]
; Nord theme ============
background = #292d3e
foreground = #c0c5ce
alert = #bd2c40
volume-min = #a3be8c
volume-med = #ebcb8b
volume-max = #bf616a
; =======================

; Gotham theme ==========
; background = #0a0f14
; foreground = #99d1ce
; alert = #d26937
; volume-min = #2aa889
; volume-med = #edb443
; volume-max = #c23127
; =======================

; INTRCPTR theme ============
;background = ${xrdb:color0:#222}
;background = #aa000000
;background-alt = #444
;foreground = ${xrdb:color7:#222}
;foreground = #fff
;foreground-alt = #555
;primary = #ffb52a
;secondary = #e60053
;alert = #bd2c40

################################################################################
################################################################################
############                         MAINBAR-I3                     ############
################################################################################
################################################################################

[bar/mainbar-i3]
;https://github.com/jaagr/polybar/wiki/Configuration

monitor = ${env:MONITOR}
;monitor-fallback = DP-1
monitor-strict = false
override-redirect = false
bottom = false
fixed-center = true
width = 100%
height = 20
;offset-x = 1%
;offset-y = 1%

background = ${colors.background}
foreground = ${colors.foreground}

; Background gradient (vertical steps)
;   background-[0-9]+ = #aarrggbb
;background-0 =

radius = 0.0
line-size = 2
line-color = #000000

border-size = 0
;border-left-size = 25
;border-right-size = 25
;border-top-size = 0
;border-bottom-size = 25
border-color = #000000

padding-left = 1
padding-right = 1

module-margin-left = 0
module-margin-right = 0

;https://github.com/jaagr/polybar/wiki/Fonts
font-0 = "UbuntuMono Nerd Font:size=10;2"
font-1 = "UbuntuMono Nerd Font:size=16;3"
font-2 = "Font Awesome 5 Free:style=Regular:pixelsize=8;1"
font-3 = "Font Awesome 5 Free:style=Solid:pixelsize=8;1"
font-4 = "Font Awesome 5 Brands:pixelsize=8;1"

modules-left = i3 xwindow
modules-right = arrow8 networkspeedup networkspeeddown arrow2 memory2 arrow3 cpu2 arrow4 pavolume arrow6 date

separator = 

;dim-value = 1.0

tray-position = right
tray-detached = false
tray-maxsize = 20
tray-background = ${colors.background}
tray-offset-x = 0
tray-offset-y = 0
tray-padding = 4
tray-scale = 1.0

#i3: Make the bar appear below windows
;wm-restack = i3
;override-redirect = true

; Enable support for inter-process messaging
; See the Messaging wiki page for more details.
enable-ipc = true

; Fallback click handlers that will be called if
; there's no matching module handler found.
click-left =
click-middle =
click-right =
scroll-up = i3wm-wsnext
scroll-down = i3wm-wsprev
double-click-left =
double-click-middle =
double-click-right =

; Requires polybar to be built with xcursor support (xcb-util-cursor)
; Possible values are:
; - default   : The default pointer as before, can also be an empty string (default)
; - pointer   : Typically in the form of a hand
; - ns-resize : Up and down arrows, can be used to indicate scrolling
cursor-click =
cursor-scroll =


################################################################################
################################################################################
############                       MODULE I3                        ############
################################################################################
################################################################################

[module/i3]
;https://github.com/jaagr/polybar/wiki/Module:-i3
type = internal/i3

; Only show workspaces defined on the same output as the bar
;
; Useful if you want to show monitor specific workspaces
; on different bars
;
; Default: false
pin-workspaces = true

; This will split the workspace name on ':'
; Default: false
strip-wsnumbers = false

; Sort the workspaces by index instead of the default
; sorting that groups the workspaces by output
; Default: false
index-sort = false

; Create click handler used to focus workspace
; Default: true
enable-click = true

; Create scroll handlers used to cycle workspaces
; Default: true
enable-scroll = true

; Wrap around when reaching the first/last workspace
; Default: true
wrapping-scroll = false

; Set the scroll cycle direction
; Default: true
reverse-scroll = false

; Use fuzzy (partial) matching on labels when assigning
; icons to workspaces
; Example: code;♚ will apply the icon to all workspaces
; containing 'code' in the label
; Default: false
fuzzy-match = false

;extra icons to choose from
;http://fontawesome.io/cheatsheet/
;       v     

ws-icon-0 =  
ws-icon-1 = 2
ws-icon-2 = 3
ws-icon-3 = 4
ws-icon-4 = 5
ws-icon-5 = 6
ws-icon-6 = 7
ws-icon-7 = 8
ws-icon-8 = 9
ws-icon-9 = 10
;ws-icon-default = " "

; Available tags:
;   <label-state> (default) - gets replaced with <label-(focused|unfocused|visible|urgent)>
;   <label-mode> (default)
format = <label-state> <label-mode>

label-mode = %mode%
label-mode-padding = 2
label-mode-foreground = #000000
label-mode-background = #FFBB00

; Available tokens:
;   %name%
;   %icon%
;   %index%
;   %output%
; Default: %icon%  %name%
; focused = Active workspace on focused monitor
label-focused = %icon% %name%
label-focused-background = ${colors.background}
label-focused-foreground = ${colors.foreground}
label-focused-underline = #AD69AF
label-focused-padding = 2

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
; unfocused = Inactive workspace on any monitor
label-unfocused = %icon% %name%
label-unfocused-padding = 2
label-unfocused-background = ${colors.background}
label-unfocused-foreground = ${colors.foreground}
label-unfocused-underline =

; visible = Active workspace on unfocused monitor
label-visible = %icon% %name%
label-visible-background = ${self.label-focused-background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = 2

; Available tokens:
;   %name%
;   %icon%
;   %index%
; Default: %icon%  %name%
; urgent = Workspace with urgency hint set
label-urgent = %icon% %name%
label-urgent-background = ${self.label-focused-background}
label-urgent-foreground = #db104e
label-urgent-padding = 2

format-foreground = ${colors.foreground}
format-background = ${colors.background}


################################################################################
###############################################################################
############                       MODULES ARROWS                     ############
################################################################################
################################################################################


[module/arrow1]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #C3E88D
content-background = #292d3e

[module/arrow2]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #82AAFF
content-background = #C3E88D

[module/arrow3]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #C792EA
content-background = #82AAFF

[module/arrow4]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #FFE585
content-background = #C792EA

[module/arrow5]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #FFE585
content-background = #FFE585

[module/arrow6]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #1D2330
content-background = #FFE585

[module/arrow7]
; grey to Blue
type = custom/text
content = "%{T2}%{T-}"
content-font = 2
content-foreground = #C3E88D
content-background = #FBBC05

[module/arrow8]
; grey to Blue
type = custom/text
content = "%{T2} %{T-}"
content-font = 2
content-foreground = #C3E88D
content-background = #292d3e 



################################################################################
###############################################################################
############                       MODULES A-Z                      ############
################################################################################
################################################################################

[module/backlight-acpi]
inherit = module/xbacklight
type = internal/backlight
card = intel_backlight
format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-prefix-foreground = #7D49B6
format-prefix-underline = #7D49B6
format-underline = #7D49B6
################################################################################

[module/backlight]
;https://github.com/jaagr/polybar/wiki/Module:-backlight

type = internal/backlight

; Use the following command to list available cards:
; $ ls -1 /sys/class/backlight/
card = intel_backlight

; Available tags:
;   <label> (default)
;   <ramp>
;   <bar>
format = <label>
format-foreground = ${colors.foreground}
format-background = ${colors.background}

; Available tokens:
;   %percentage% (default)
label = %percentage%%

; Only applies if <ramp> is used
ramp-0 = 🌕
ramp-1 = 🌔
ramp-2 = 🌓
ramp-3 = 🌒
ramp-4 = 🌑

; Only applies if <bar> is used
bar-width = 10
bar-indicator = |
bar-fill = ─
bar-empty = ─


[module/cpu1]
;https://github.com/jaagr/polybar/wiki/Module:-cpu
type = internal/cpu
; Seconds to sleep between updates
; Default: 1
interval = 1
format-foreground = ${colors.foreground}
format-background = ${colors.background}
;   
format-prefix = " "
format-prefix-foreground = #cd1f3f
format-underline = #645377

; Available tags:
;   <label> (default)
;   <bar-load>
;   <ramp-load>
;   <ramp-coreload>
format = <label> <ramp-coreload>

format-padding = 2

; Available tokens:
;   %percentage% (default) - total cpu load
;   %percentage-cores% - load percentage for each core
;   %percentage-core[1-9]% - load percentage for specific core
label-font = 3
label = Cpu %percentage:3%%
ramp-coreload-0 = ▁
ramp-coreload-0-font = 3
ramp-coreload-0-foreground = #aaff77
ramp-coreload-1 = ▂
ramp-coreload-1-font = 3
ramp-coreload-1-foreground = #aaff77
ramp-coreload-2 = ▃
ramp-coreload-2-font = 3
ramp-coreload-2-foreground = #aaff77
ramp-coreload-3 = ▄
ramp-coreload-3-font = 3
ramp-coreload-3-foreground = #aaff77
ramp-coreload-4 = ▅
ramp-coreload-4-font = 3
ramp-coreload-4-foreground = #fba922
ramp-coreload-5 = ▆
ramp-coreload-5-font = 3
ramp-coreload-5-foreground = #fba922
ramp-coreload-6 = ▇
ramp-coreload-6-font = 3
ramp-coreload-6-foreground = #ff5555
ramp-coreload-7 = █
ramp-coreload-7-font = 3
ramp-coreload-7-foreground = #ff5555

################################################################################

[module/cpu2]
;https://github.com/jaagr/polybar/wiki/Module:-cpu
type = internal/cpu
; Seconds to sleep between updates
; Default: 1
interval = 1
format-foreground = ${colors.background}
format-background = #C792EA
format-prefix = " "
format-prefix-foreground = ${colors.background}

label-font = 1

; Available tags:
;   <label> (default)
;   <bar-load>
;   <ramp-load>
;   <ramp-coreload>
format = <label>


; Available tokens:
;   %percentage% (default) - total cpu load
;   %percentage-cores% - load percentage for each core
;   %percentage-core[1-9]% - load percentage for specific core
label = Cpu %percentage:3%%

################################################################################

[module/date]
;https://github.com/jaagr/polybar/wiki/Module:-date
type = internal/date
; Seconds to sleep between updates
interval = 5
; See "http://en.cppreference.com/w/cpp/io/manip/put_time" for details on how to format the date string
; NOTE: if you want to use syntax tags here you need to use %%{...}
date = " %a %b %d, %Y"
date-alt = " %a %b %d, %Y"
time = %l:%M%p
time-alt = %l:%M%p
format-prefix = " "
format-prefix-foreground = ${colors.foreground}
format-foreground = ${colors.foreground}
format-background = ${colors.background}
label = "%date% %time% "

################################################################################

[module/load-average]
type = custom/script
exec = uptime | grep -ohe 'load average[s:][: ].*' | awk '{ print $3" "$4" "$5"," }' | sed 's/,//g'
interval = 100

;HOW TO SET IT MINIMAL 10 CHARACTERS - HIDDEN BEHIND SYSTEM ICONS
;label = %output%
label = %output:10%

format-foreground = ${colors.foreground}
format-background = ${colors.background}
format-prefix = "  "
format-prefix-foreground = #62FF00
format-underline = #62FF00

################################################################################

[module/memory1]
;https://github.com/jaagr/polybar/wiki/Module:-memory
type = internal/memory
interval = 1
; Available tokens:
;   %percentage_used% (default)
;   %percentage_free%
;   %gb_used%
;   %gb_free%
;   %gb_total%
;   %mb_used%
;   %mb_free%
;   %mb_total%
label = %percentage_used%%
bar-used-indicator =
bar-used-width = 10
bar-used-foreground-0 = #3384d0
bar-used-fill = 
bar-used-empty = 
bar-used-empty-foreground = #ffffff

format = <label> <bar-used>
format-prefix = "  "
format-prefix-foreground = #3384d0
format-underline = #4B5665
format-foreground = ${colors.foreground}
format-background = ${colors.background}

################################################################################

[module/memory2]
;https://github.com/jaagr/polybar/wiki/Module:-memory
type = internal/memory
interval = 1
; Available tokens:
;   %percentage_used% (default)
;   %percentage_free%
;   %gb_used%
;   %gb_free%
;   %gb_total%
;   %mb_used%
;   %mb_free%
;   %mb_total%
label = %percentage_used%%

format = Mem <label>
format-prefix = " "
format-prefix-foreground = #292d3e
format-foreground = #292d3e
format-background = #82AAFF

################################################################################

[module/memory3]
;https://github.com/jaagr/polybar/wiki/Module:-memory
type = internal/memory
interval = 1
; Available tokens:
;   %percentage_used% (default)
;   %percentage_free%
;   %gb_used%
;   %gb_free%
;   %gb_total%
;   %mb_used%
;   %mb_free%
;   %mb_total%
label = %gb_used%/%gb_free%

format = Mem <label>
format-prefix = "  "
format-prefix-foreground = #3384d0
format-underline = #3384d0
format-foreground = ${colors.foreground}
format-background = ${colors.background}

################################################################################

[module/networkspeedup]
;https://github.com/jaagr/polybar/wiki/Module:-network
type = internal/network
;interface = wlp3s0
;interface = enp14s0
;interface = enp0s31f6
interface = ens1
label-connected = "%upspeed:7%"
format-connected = <label-connected>
format-connected-prefix = " "
format-connected-prefix-foreground = #292d3e
format-connected-foreground = #292d3e
format-connected-background = #C3E88D

################################################################################

[module/networkspeeddown]
;https://github.com/jaagr/polybar/wiki/Module:-network
type = internal/network
;interface = wlp3s0
;interface = enp14s0
;interface = enp0s31f6
interface = ens1
label-connected = "%downspeed:7%"
format-connected = <label-connected>
format-connected-prefix = "  "
format-connected-prefix-foreground = #292d3e
format-connected-foreground = #292d3e
format-connected-background = #C3E88D

################################################################################

[module/pavolume]
type = custom/script
tail = true
label = %output%
exec = ~/.config/polybar/scripts/pavolume.sh --listen
click-right = exec pavucontrol
click-left = ~/.config/polybar/scripts/pavolume.sh --togmute
scroll-up = ~/.config/polybar/scripts/pavolume.sh --up
scroll-down = ~/.config/polybar/scripts/pavolume.sh --down
format-foreground = ${colors.background}
format-background = #FFE585

###############################################################################

[module/sep]
; alternative separator
type = custom/text
content = 
content-foreground = ${colors.foreground}
content-background =  ${colors.background}
format-foreground = ${colors.foreground}
format-background = ${colors.background}

################################################################################

[module/volume]
;https://github.com/jaagr/polybar/wiki/Module:-volume
type = internal/volume
format-volume = "<label-volume>  <bar-volume>"

label-volume = " "
label-volume-foreground = #40ad4b
label-muted = muted

bar-volume-width = 10
bar-volume-foreground-0 = #40ad4b
bar-volume-foreground-1 = #40ad4b
bar-volume-foreground-2 = #40ad4b
bar-volume-foreground-3 = #40ad4b
bar-volume-foreground-4 = #40ad4b
bar-volume-foreground-5 = #40ad4b
bar-volume-foreground-6 = #40ad4b
bar-volume-gradient = false
bar-volume-indicator = 
bar-volume-indicator-font = 2
bar-volume-fill = 
bar-volume-fill-font = 2
bar-volume-empty = 
bar-volume-empty-font = 2
bar-volume-empty-foreground = ${colors.foreground}
format-volume-foreground = ${colors.foreground}
format-volume-background = ${colors.background}
format-muted-prefix = "  "
format-muted-prefix-foreground = "#ff0000"
format-muted-foreground = ${colors.foreground}
format-muted-background = ${colors.background}


[module/wired-network]
;https://github.com/jaagr/polybar/wiki/Module:-network
type = internal/network
interface = ens1
;interface = enp14s0
interval = 3.0

; Available tokens:
;   %ifname%    [wireless+wired]
;   %local_ip%  [wireless+wired]
;   %essid%     [wireless]
;   %signal%    [wireless]
;   %upspeed%   [wireless+wired]
;   %downspeed% [wireless+wired]
;   %linkspeed% [wired]
; Default: %ifname% %local_ip%
label-connected =  %local_ip% 
label-disconnected = %ifname% disconnected

format-connected-foreground = ${colors.foreground}
format-connected-background = ${colors.background}
format-connected-underline = #55aa55
format-connected-prefix = " "
format-connected-prefix-foreground = #55aa55
format-connected-prefix-background = ${colors.background}

format-disconnected = <label-disconnected>
format-disconnected-underline = ${colors.alert}
label-disconnected-foreground = ${colors.foreground}

################################################################################

[module/xbacklight]
;https://github.com/jaagr/polybar/wiki/Module:-xbacklight
type = internal/xbacklight
format = <label> <bar>
format-prefix = "   "
format-prefix-foreground = ${colors.foreground}
format-prefix-background = ${colors.background}
format-prefix-underline = #9f78e1
format-underline = #9f78e1
label = %percentage%%
bar-width = 10
bar-indicator = 
bar-indicator-foreground = #fff
bar-indicator-font = 2
bar-fill = 
bar-fill-font = 2
bar-fill-foreground = #9f78e1
bar-empty = 
bar-empty-font = 2
bar-empty-foreground = #fff
format-foreground = ${colors.foreground}
format-background = ${colors.background}

################################################################################

[module/xkeyboard]
;https://github.com/jaagr/polybar/wiki/Module:-xkeyboard
type = internal/xkeyboard
blacklist-0 = num lock

format-prefix = " "
format-prefix-foreground = ${colors.foreground}
format-prefix-background = ${colors.background}
format-prefix-underline = #3ecfb2
format-foreground = ${colors.foreground}
format-background = ${colors.background}

label-layout = %layout%
label-layout-underline = #3ecfb2
label-indicator-padding = 2
label-indicator-margin = 1
label-indicator-background = ${colors.background}
label-indicator-underline = ${colors.foreground}

################################################################################

[module/xwindow]
;https://github.com/jaagr/polybar/wiki/Module:-xwindow
type = internal/xwindow

; Available tokens:
;   %title%
; Default: %title%
label = %title%
label-maxlen = 50

format-prefix = "  "
format-foreground = ${colors.foreground}
format-background = ${colors.background}

###############################################################################
# vim:ft=dosini
